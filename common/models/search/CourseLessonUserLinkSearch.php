<?php

namespace emilasp\course\common\models\search;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use emilasp\course\common\models\CourseLessonUserLink;

/**
 * CourseLessonUserLinkSearch represents the model behind the search form of `emilasp\course\common\models\CourseLessonUserLink`.
 */
class CourseLessonUserLinkSearch extends CourseLessonUserLink
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'lesson_id', 'user_id', 'score', 'status'], 'integer'],
            [['tasks', 'start_at', 'end_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = CourseLessonUserLink::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'sort' => ['defaultOrder' => ['id' => SORT_DESC]]
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id' => $this->id,
            'lesson_id' => $this->lesson_id,
            'user_id' => $this->user_id,
            'score' => $this->score,
            'status' => $this->status,
            'start_at' => $this->start_at,
            'end_at' => $this->end_at,
        ]);

        $query->andFilterWhere(['like', 'tasks', $this->tasks]);

        return $dataProvider;
    }
}
