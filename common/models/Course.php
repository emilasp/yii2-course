<?php

namespace emilasp\course\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\core\helpers\StringHelper;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\behaviors\FileSingleBehavior;
use emilasp\media\models\File;
use emilasp\social\common\behaviors\ViewsBehavior;
use emilasp\social\frontend\behaviors\CommentBehavior;
use emilasp\social\frontend\behaviors\RatingBehavior;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "course_course".
 *
 * @property integer          $id
 * @property integer          $image_id
 * @property string           $code
 * @property string           $name
 * @property string           $description
 * @property integer          $status
 * @property integer          $views
 * @property integer          $rating
 * @property string           $start_at
 * @property string           $created_at
 * @property string           $updated_at
 * @property integer          $created_by
 * @property integer          $updated_by
 *
 * @property CourseBlock[]    $blocks
 * @property File             $image
 * @property User             $createdBy
 * @property User             $updatedBy
 * @property CourseUserLink[] $courseUserLinks
 * @property CourseLesson[]   $lessons
 */
class Course extends ActiveRecord
{
    public $imageUpload;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_course';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([

            'views'    => [
                'class' => ViewsBehavior::className(),
            ],
            'comments' => [
                'class' => CommentBehavior::className(),
            ],
            'rating'   => [
                'class' => RatingBehavior::className(),
            ],

            'files' => [
                'class'     => FileBehavior::className(),
                'attribute' => 'files',
            ],
            'image' => [
                'class'         => FileSingleBehavior::className(),
                'attribute'     => 'image_id',
                'formAttribute' => 'imageUpload',
            ],

            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'status'], 'required'],
            [['image_id', 'status', 'views', 'rating', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['start_at', 'created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 100],
            [
                ['image_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => File::className(),
                'targetAttribute' => ['image_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],

            [['views', 'rating'], 'integer'],

            [['imageUpload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('course', 'ID'),
            'image_id'    => Yii::t('course', 'Image ID'),
            'code'        => Yii::t('course', 'Code'),
            'name'        => Yii::t('course', 'Name'),
            'description' => Yii::t('course', 'Description'),
            'status'      => Yii::t('course', 'Status'),
            'views'       => Yii::t('course', 'Views'),
            'rating'      => Yii::t('course', 'Rating'),
            'start_at'    => Yii::t('course', 'Start At'),
            'created_at'  => Yii::t('course', 'Created At'),
            'updated_at'  => Yii::t('course', 'Updated At'),
            'created_by'  => Yii::t('course', 'Created By'),
            'updated_by'  => Yii::t('course', 'Updated By'),
        ];
    }

    /**
     * Extra fields
     * @return array
     */
    public function extraFields()
    {
        return [
            'blocks',
            'lessons',
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlocks()
    {
        return $this->hasMany(CourseBlock::className(), ['course_id' => 'id'])->orderBy(['course_block.id' => SORT_ASC]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourseUserLinks()
    {
        return $this->hasMany(CourseUserLink::className(), ['course_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasMany(CourseLesson::className(), ['course_id' => 'id']);
    }

    /**
     * По событию перед сохранением
     *
     * @param bool $insert
     * @return mixed
     */
    public function beforeSave($insert)
    {
        $this->setCode();

        return parent::beforeSave($insert);
    }

    /**
     * Устанавливаем код
     */
    private function setCode(): void
    {
        if (!$this->code) {
            $this->code = StringHelper::str2url($this->name);
        }
    }
}
