<?php

namespace emilasp\course\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\media\behaviors\FileSingleBehavior;
use emilasp\media\models\File;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "course_block".
 *
 * @property integer        $id
 * @property integer        $course_id
 * @property integer        $image_id
 * @property string         $name
 * @property string         $description
 * @property integer        $status
 * @property string         $created_at
 * @property string         $updated_at
 * @property integer        $created_by
 * @property integer        $updated_by
 *
 * @property Course         $course
 * @property File           $image
 * @property User           $createdBy
 * @property User           $updatedBy
 * @property CourseLesson[] $courseLessons
 */
class CourseBlock extends ActiveRecord
{
    public $imageUpload;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_block';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([

            'image'              => [
                'class'         => FileSingleBehavior::className(),
                'attribute'     => 'image_id',
                'formAttribute' => 'imageUpload',
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'name', 'description', 'status'], 'required'],
            [['course_id', 'image_id', 'status', 'created_by', 'updated_by'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [
                ['course_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Course::className(),
                'targetAttribute' => ['course_id' => 'id']
            ],
            [
                ['image_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => File::className(),
                'targetAttribute' => ['image_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'          => Yii::t('course', 'ID'),
            'course_id'   => Yii::t('course', 'Course ID'),
            'image_id'    => Yii::t('course', 'Image ID'),
            'name'        => Yii::t('course', 'Name'),
            'description' => Yii::t('course', 'Description'),
            'status'      => Yii::t('course', 'Status'),
            'created_at'  => Yii::t('course', 'Created At'),
            'updated_at'  => Yii::t('course', 'Updated At'),
            'created_by'  => Yii::t('course', 'Created By'),
            'updated_by'  => Yii::t('course', 'Updated By'),
        ];
    }


    /**
     * Extra fields
     * @return array
     */
    public function extraFields()
    {
        return [
            'lessons',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessons()
    {
        return $this->hasMany(CourseLesson::className(), ['block_id' => 'id'])->orderBy(['course_lesson.id' => SORT_ASC]);
    }
}
