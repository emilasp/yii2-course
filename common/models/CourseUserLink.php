<?php

namespace emilasp\course\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\media\models\File;
use emilasp\users\common\models\User;
use Yii;

/**
 * This is the model class for table "course_course_user_link".
 *
 * @property integer              $id
 * @property integer              $course_id
 * @property integer              $user_id
 * @property integer              $pay_id
 * @property integer              $status
 * @property string               $start_at
 * @property string               $end_at
 *
 * @property Course               $course
 * @property User                 $user
 * @property CourseLessonUserLink $userLessons
 */
class CourseUserLink extends ActiveRecord
{
    //Идет, завершен, закрыт, ожидает оплаты
    public const STATUS_CLOSE     = 0;
    public const STATUS_WORK      = 1;
    public const STATUS_END       = 2;
    public const STATUS_WAIT_PAYD = 3;

    public static $statuses = [
        self::STATUS_WORK      => 'Work',
        self::STATUS_END       => 'End',
        self::STATUS_WAIT_PAYD => 'Wait Payd',
        self::STATUS_CLOSE     => 'Close',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_course_user_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'user_id', 'status'], 'required'],
            [['course_id', 'user_id', 'pay_id', 'status'], 'integer'],
            [['start_at', 'end_at'], 'safe'],
            [
                ['course_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Course::className(),
                'targetAttribute' => ['course_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('course', 'ID'),
            'course_id' => Yii::t('course', 'Course ID'),
            'user_id'   => Yii::t('course', 'User ID'),
            'pay_id'    => Yii::t('course', 'Pay ID'),
            'status'    => Yii::t('course', 'Status'),
            'start_at'  => Yii::t('course', 'Start At'),
            'end_at'    => Yii::t('course', 'End At'),
        ];
    }

    /**
     *  Fields
     * @return array
     */
    public function fields()
    {
        return [
            'id',
            'course_id',
            'user_id',
            'pay_id',
            'status',
            'start_at',
            'end_at',
        ];
    }

    /**
     * Extra fields
     * @return array
     */
    public function extraFields()
    {
        return [
            'course',
            'image'   => function ($model) {
                return $model->course->image ? $model->course->image->getUrl(File::SIZE_MED) : '';
            },
            'lessons' => function ($model) {
                return $model->userLessons;
            },
            'passed'  => function ($model) {
                return $this->getPassed();
            },
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUserLessons()
    {
        return $this->hasMany(CourseLessonUserLink::className(), ['user_id' => 'user_id']);
    }

    /**
     * Счиатем процент прохождения курса
     *
     * @return float
     */
    private function getPassed(int $precision = 0): float
    {
        $passed = 0;
        $all    = $this->course->lessons ? count($this->course->lessons) : 1;

        foreach ($this->userLessons as $lessonLink) {
            if ($lessonLink->course->id === $this->course_id) {
                if ($lessonLink->status === CourseLessonUserLink::STATUS_END) {
                    $passed++;
                }
            }
        }

        return round(($passed / $all) * 100, $precision);
    }
}
