<?php

namespace emilasp\course\common\models;

use emilasp\core\components\base\ActiveRecord;
use emilasp\media\models\File;
use emilasp\users\common\models\User;
use Yii;

/**
 * This is the model class for table "course_lesson_user_link".
 *
 * @property integer      $id
 * @property integer      $lesson_id
 * @property integer      $user_id
 * @property array        $tasks
 * @property integer      $score
 * @property integer      $status
 * @property string       $start_at
 * @property string       $end_at
 *
 * @property CourseLesson $lesson
 * @property Course       $course
 * @property User         $user
 */
class CourseLessonUserLink extends ActiveRecord
{
    public const STATUS_WORK = 1;
    public const STATUS_END  = 2;

    public static $statuses = [
        self::STATUS_WORK => 'Work',
        self::STATUS_END  => 'End',
    ];

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_lesson_user_link';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['lesson_id', 'user_id', 'status'], 'required'],
            [['lesson_id', 'user_id', 'score', 'status'], 'integer'],
            [['tasks'], 'string'],
            [['start_at', 'end_at'], 'safe'],
            [
                ['lesson_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CourseLesson::className(),
                'targetAttribute' => ['lesson_id' => 'id']
            ],
            [
                ['user_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['user_id' => 'id']
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'        => Yii::t('course', 'ID'),
            'lesson_id' => Yii::t('course', 'Lesson ID'),
            'user_id'   => Yii::t('course', 'User ID'),
            'tasks'     => Yii::t('course', 'Tasks'),
            'score'     => Yii::t('course', 'Score'),
            'status'    => Yii::t('course', 'Status'),
            'start_at'  => Yii::t('course', 'Start At'),
            'end_at'    => Yii::t('course', 'End At'),
        ];
    }

    /**
     * Extra fields
     * @return array
     */
    public function extraFields()
    {
        return [
            'lesson',
            'image' => function ($model) {
                return $model->lesson->image ? $model->lesson->image->getUrl(File::SIZE_MED) : '';
            },
        ];
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLesson()
    {
        return $this->hasOne(CourseLesson::class, ['id' => 'lesson_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::class, ['id' => 'course_id'])
            ->viaTable('course_lesson', ['id' => 'lesson_id']);
    }
}
