<?php

namespace emilasp\course\common\models;

use emilasp\core\behaviors\data\JsonFieldConvertBehavior;
use emilasp\core\components\base\ActiveRecord;
use emilasp\core\extensions\CkeEditor\behaviors\SaveTmpImagesBehavior;
use emilasp\core\helpers\StringHelper;
use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\behaviors\FileSingleBehavior;
use emilasp\media\models\File;
use emilasp\social\common\behaviors\ViewsBehavior;
use emilasp\social\frontend\behaviors\CommentBehavior;
use emilasp\social\frontend\behaviors\RatingBehavior;
use emilasp\users\common\models\User;
use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "course_lesson".
 *
 * @property integer                $id
 * @property string                 $code
 * @property integer                $course_id
 * @property integer                $block_id
 * @property integer                $image_id
 * @property string                 $name
 * @property string                 $entry
 * @property string                 $content
 * @property string                 $conclusion
 * @property string                 $entry_media
 * @property string                 $content_media
 * @property string                 $conclusion_media
 * @property array                  $tasks
 * @property integer                $views
 * @property integer                $rating
 * @property integer                $type
 * @property integer                $type_content
 * @property integer                $status
 * @property string                 $created_at
 * @property string                 $updated_at
 * @property integer                $created_by
 * @property integer                $updated_by
 *
 * @property CourseBlock            $block
 * @property Course                 $course
 * @property File                   $image
 * @property User                   $createdBy
 * @property User                   $updatedBy
 * @property CourseLessonUserLink[] $lessonUserLinks
 * @property CourseLessonUserLink   $lessonCurrentUserLink
 */
class CourseLesson extends ActiveRecord
{
    public const TYPE_LESSON = 1;
    public const TYPE_TEST   = 2;
    public const TYPE_GAME   = 3;

    public static $types = [
        self::TYPE_LESSON => 'Lesson',
        self::TYPE_TEST   => 'Test',
    ];

    public const TYPE_CONTENT_TEXT  = 1;
    public const TYPE_CONTENT_VIDEO = 2;
    public const TYPE_CONTENT_AUDIO = 3;
    public const TYPE_CONTENT_MIXED = 4;

    public static $typeContents = [
        self::TYPE_CONTENT_TEXT  => 'Text',
        self::TYPE_CONTENT_VIDEO => 'Video',
        self::TYPE_CONTENT_AUDIO => 'Audio',
        self::TYPE_CONTENT_MIXED => 'Mixed',
    ];

    public $imageUpload;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'course_lesson';
    }

    /**
     * @return array
     */
    public function behaviors()
    {
        return ArrayHelper::merge([
            'json'    => [
                'class' => JsonFieldConvertBehavior::class,
                'attributes' => ['tasks', 'entry_media', 'content_media', 'conclusion_media']
            ],

            'views'    => [
                'class' => ViewsBehavior::className(),
            ],
            'comments' => [
                'class' => CommentBehavior::className(),
            ],
            'rating'   => [
                'class' => RatingBehavior::className(),
            ],

            'files' => [
                'class'     => FileBehavior::className(),
                'attribute' => 'files',
            ],
            'image' => [
                'class'         => FileSingleBehavior::className(),
                'attribute'     => 'image_id',
                'formAttribute' => 'imageUpload',
            ],

            'cke_tmp_image_save' => [
                'class'      => SaveTmpImagesBehavior::className(),
                'attributes' => ['entry', 'content', 'conclusion'],
            ],
            [
                'class' => TimestampBehavior::className(),
                'value' => new Expression('NOW()'),
            ],
            [
                'class' => BlameableBehavior::className(),
                'value' => function () {
                    return !isset(Yii::$app->user) ? 1 : Yii::$app->user->id;
                },
            ],
        ], parent::behaviors());
    }


    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['course_id', 'block_id', 'name', 'type', 'type_content', 'status'], 'required'],
            [
                [
                    'course_id',
                    'block_id',
                    'image_id',
                    'views',
                    'rating',
                    'type',
                    'type_content',
                    'status',
                    'created_by',
                    'updated_by'
                ],
                'integer'
            ],
            [['entry', 'content', 'conclusion', 'entry_media', 'content_media', 'conclusion_media', 'tasks'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['code'], 'string', 'max' => 100],
            [
                ['block_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => CourseBlock::className(),
                'targetAttribute' => ['block_id' => 'id']
            ],
            [
                ['course_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => Course::className(),
                'targetAttribute' => ['course_id' => 'id']
            ],
            [
                ['image_id'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => File::className(),
                'targetAttribute' => ['image_id' => 'id']
            ],
            [
                ['created_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['created_by' => 'id']
            ],
            [
                ['updated_by'],
                'exist',
                'skipOnError'     => true,
                'targetClass'     => User::className(),
                'targetAttribute' => ['updated_by' => 'id']
            ],

            [['views', 'rating'], 'integer'],

            [['imageUpload'], 'file', 'skipOnEmpty' => true, 'extensions' => 'png, jpg'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'               => Yii::t('course', 'ID'),
            'course_id'        => Yii::t('course', 'Course ID'),
            'block_id'         => Yii::t('course', 'Block ID'),
            'image_id'         => Yii::t('course', 'Image ID'),
            'name'             => Yii::t('course', 'Name'),
            'entry'            => Yii::t('course', 'Entry'),
            'content'          => Yii::t('course', 'Content'),
            'conclusion'       => Yii::t('course', 'Conclusion'),
            'entry_media'      => Yii::t('course', 'Entry media'),
            'content_media'    => Yii::t('course', 'Content media'),
            'conclusion_media' => Yii::t('course', 'Conclusion media'),

            'tasks'        => Yii::t('course', 'Tasks'),
            'views'        => Yii::t('course', 'Views'),
            'rating'       => Yii::t('course', 'Rating'),
            'type'         => Yii::t('course', 'Type'),
            'type_content' => Yii::t('course', 'Type Content'),
            'status'       => Yii::t('course', 'Status'),
            'created_at'   => Yii::t('course', 'Created At'),
            'updated_at'   => Yii::t('course', 'Updated At'),
            'created_by'   => Yii::t('course', 'Created By'),
            'updated_by'   => Yii::t('course', 'Updated By'),
        ];
    }

    /**
     * Extra fields
     * @return array
     */
    public function extraFields()
    {
        return [
            'lessonCurrentUserLink',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBlock()
    {
        return $this->hasOne(CourseBlock::className(), ['id' => 'block_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCourse()
    {
        return $this->hasOne(Course::className(), ['id' => 'course_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getImage()
    {
        return $this->hasOne(File::className(), ['id' => 'image_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonUserLinks()
    {
        return $this->hasMany(CourseLessonUserLink::className(), ['lesson_id' => 'id']);
    }


    /**
     * @return \yii\db\ActiveQuery
     */
    public function getLessonCurrentUserLink()
    {
        $userId = Yii::$app->user->id;
        return $this->hasOne(CourseLessonUserLink::className(), ['lesson_id' => 'id'])
            ->where(['user_id' => $userId]);
    }

    /**
     * По событию перед сохранением
     *
     * @param bool $insert
     * @return mixed
     */
    public function beforeSave($insert)
    {
        $this->setCode();

        return parent::beforeSave($insert);
    }

    /**
     * Устанавливаем код
     */
    private function setCode(): void
    {
        if (!$this->code) {
            $this->code = StringHelper::str2url($this->name);
        }
    }
}
//0x5320056bf515f7910d9d9fa42973aaa6c41e73d5