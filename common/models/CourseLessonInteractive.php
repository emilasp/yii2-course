<?php

namespace emilasp\course\common\models;

use emilasp\core\behaviors\data\JsonFieldConvertBehavior;
use emilasp\core\components\base\ActiveRecord;
use emilasp\core\extensions\CkeEditor\behaviors\SaveTmpImagesBehavior;
use emilasp\core\helpers\StringHelper;
use emilasp\json\behaviors\JsonFieldBehavior;
use emilasp\media\behaviors\FileBehavior;
use emilasp\media\behaviors\FileSingleBehavior;
use emilasp\media\models\File;
use emilasp\social\common\behaviors\ViewsBehavior;
use emilasp\social\frontend\behaviors\CommentBehavior;
use emilasp\social\frontend\behaviors\RatingBehavior;
use emilasp\users\common\models\User;
use Yii;
use yii\base\Model;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\Expression;
use yii\helpers\ArrayHelper;


class CourseLessonInteractive extends Model
{
    public const INTERACTIVE_TYPE_TEST                   = 'test';
    public const INTERACTIVE_TEST_QUESTION_TYPE_CHECKBOX = 'checkbox';
    public const INTERACTIVE_TEST_QUESTION_TYPE_RADIO    = 'radio';
    public const INTERACTIVE_TEST_QUESTION_TYPE_FREE     = 'free';

    public const TEST_EMPTY = [
        'type'   => self::INTERACTIVE_TYPE_TEST,
        'config' => [
            'code' => 'test_free',
            'passage_cost_sum' => 10,
            'items'   => [
                [
                    'type'       => self::INTERACTIVE_TEST_QUESTION_TYPE_CHECKBOX,
                    'annotation' => 'Annotation',
                    'text'       => 'Question',
                    'cost'       => 1,
                    'answers'    => ['one', 'two', 'free'],
                    'pattern'    => '/one/',
                ],
                [
                    'type'       => self::INTERACTIVE_TEST_QUESTION_TYPE_FREE,
                    'annotation' => 'Annotation',
                    'text'       => 'Question',
                    'cost'       => 3,
                    'answers'    => ['one', 'two', 'free'],
                    'pattern'    => '/one/',
                ],
            ],
        ],
    ];

}
