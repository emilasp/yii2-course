<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
/* @var $this yii\web\View */
/* @var $searchModel emilasp\course\common\models\search\CourseLessonSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = Yii::t('course', 'Course Lessons');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-lesson-index box box-primary">

    <div class="box-header with-border text-right">
        <?= Html::a(Html::tag('i', '', ['class' => 'fa fa-plus']) . ' ' . Yii::t('site', 'Create'), ['create'], [
            'class' => 'btn btn-success btn-flat'
        ]) ?>
    </div>

    <div class="box-body table-responsive no-padding">
        <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <?php Pjax::begin(); ?>

        <?= GridView::widget([
            'dataProvider' => $dataProvider,
            'filterModel' => $searchModel,
            'layout' => "{items}\n{summary}\n{pager}",
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                'id',
                'course_id',
                'block_id',
                'image_id',
                'name',
                // 'entry:ntext',
                // 'content:ntext',
                // 'conclusion:ntext',
                // 'tasks',
                // 'views',
                // 'rating',
                // 'type',
                // 'type_content',
                // 'status',
                // 'created_at',
                // 'updated_at',
                // 'created_by',
                // 'updated_by',

                ['class' => 'yii\grid\ActionColumn'],
            ],
        ]); ?>

            <?php Pjax::end(); ?>

    </div>

</div>
