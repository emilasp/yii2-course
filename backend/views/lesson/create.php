<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseLesson */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('course', 'Course Lessons'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-lesson-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
