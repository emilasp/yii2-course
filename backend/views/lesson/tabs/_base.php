<?php

use emilasp\course\common\models\Course;
use emilasp\course\common\models\CourseBlock;
use emilasp\course\common\models\CourseLesson;
use emilasp\media\models\File;
use yii\helpers\Html;

?>

<div id="base" class="tab-pane active">
    <?php if ($model->image) : ?>
        <?= Html::a(
            Html::img($model->image->getUrl(File::SIZE_ICO), [
                'class'    => 'img-thumbnail media-object',
                'data-src' => $model->image->getUrl(File::SIZE_MAX),
                'alt'      => $model->image->title,
            ]),
            $model->image->getUrl(File::SIZE_MAX),
            [
                'data-jbox-image' => 'gl',
                'data-pjax'       => 0,
            ]
        ) ?>
    <?php else : ?>
        <?= Html::img(File::getNoImageUrl(File::SIZE_ICO), ['class' => 'img-thumbnail media-object']); ?>
    <?php endif ?>

    <?= $form->field($model, 'imageUpload')->fileInput()->label(false) ?>

    <div class="row">
        <div class="col-md-6">

            <div class="row">
                <div class="col-md-9">
                    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
                </div>
                <div class="col-md-3">
                    <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'course_id')->dropDownList(Course::find()->map()->all()) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'block_id')->dropDownList(CourseBlock::find()->map()->all()) ?>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'type')->dropDownList(CourseLesson::$types) ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'type_content')->dropDownList(CourseLesson::$typeContents) ?>
                </div>
            </div>

            <?= $form->field($model, 'status')->dropDownList(CourseLesson::$statuses) ?>

        </div>
        <div class="col-md-6">

            <?= $form->field($model, 'views')->textInput() ?>

            <?= $form->field($model, 'rating')->textInput() ?>

        </div>
    </div>

</div>
