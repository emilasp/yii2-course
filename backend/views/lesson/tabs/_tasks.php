<?php

use emilasp\course\common\models\CourseLessonInteractive;
use kdn\yii2\JsonEditor;use yii\helpers\VarDumper;

?>

<div id="tasks" class="tab-pane fade">

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'tasks')->widget(
                JsonEditor::class, ['clientOptions' => ['modes' => ['code', 'tree']]]
            ) ?>
        </div>
        <div class="col-md-6">
            <h3>Test</h3>

            <?= VarDumper::dumpAsString([CourseLessonInteractive::TEST_EMPTY], 100, true) ?>

        </div>
    </div>

</div>
