<?php

use dosamigos\ckeditor\CKEditor;
use kdn\yii2\JsonEditor;

?>

<div id="contents" class="tab-pane fade">

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options'       => ['rows' => 3],
        'preset'        => 'standart',
        'clientOptions' => [
            'filebrowserUploadUrl' => '/media/file/cke-upload'
        ]
    ]) ?>

    <?= $form->field($model, 'content_media')->widget(
        JsonEditor::class, ['clientOptions' => ['modes' => ['code', 'tree']]]
    ) ?>

</div>
