<?php

use dosamigos\ckeditor\CKEditor;
use kdn\yii2\JsonEditor;

?>

<div id="entry" class="tab-pane fade">

    <?= $form->field($model, 'entry')->widget(CKEditor::className(), [
        'options'       => ['rows' => 3],
        'preset'        => 'basic',
        'clientOptions' => [
            'filebrowserUploadUrl' => '/media/file/cke-upload'
        ]
    ]) ?>

    <?= $form->field($model, 'entry_media')->widget(
        JsonEditor::class, ['clientOptions' => ['modes' => ['code', 'tree']]]
    ) ?>

</div>
