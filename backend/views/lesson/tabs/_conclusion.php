<?php

use dosamigos\ckeditor\CKEditor;
use kdn\yii2\JsonEditor;

?>

<div id="conclusion" class="tab-pane fade">

    <?= $form->field($model, 'conclusion')->widget(CKEditor::className(), [
        'options'       => ['rows' => 3],
        'preset'        => 'basic',
        'clientOptions' => [
            'filebrowserUploadUrl' => '/media/file/cke-upload'
        ]
    ]) ?>

    <?= $form->field($model, 'conclusion_media')->widget(
        JsonEditor::class, ['clientOptions' => ['modes' => ['code', 'tree']]]
    ) ?>

</div>
