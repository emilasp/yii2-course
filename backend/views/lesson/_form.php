<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseLesson */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-lesson-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body">

        <?= $form->errorSummary($model); ?>


        <div class="row">
            <div class="col-md-2">
                <ul class="nav nav-pills flex-column">
                    <li>
                        <a data-toggle="tab" href="#base" class="nav-link active"><?= Yii::t('site', 'Tab Base') ?></a>
                    </li>
                    <li role="presentation">
                        <a aria-controls="entry" role="tab" data-toggle="tab" href="#entry" class="nav-link">
                            <?= Yii::t('course', 'Entry') ?>
                        </a>
                    </li>
                    <li role="presentation">
                        <a aria-controls="contents" role="tab" data-toggle="tab" href="#contents" class="nav-link">
                            <?= Yii::t('course', 'Content') ?>
                        </a>
                    </li>
                    <li role="presentation">
                        <a aria-controls="conclusion" role="tab" data-toggle="tab" href="#conclusion" class="nav-link">
                            <?= Yii::t('course', 'Conclusion') ?>
                        </a>
                    </li>


                    <li role="presentation">
                        <a aria-controls="tasks" role="tab" data-toggle="tab" href="#tasks" class="nav-link">
                            <?= Yii::t('course', 'Tasks') ?>
                        </a>
                    </li>
                </ul>
            </div>

            <div class="col-md-10">
                <div class="tab-content">
                    <?= $this->render('tabs/_base', ['form' => $form, 'model' => $model]) ?>
                    <?= $this->render('tabs/_tasks', ['form' => $form, 'model' => $model]) ?>
                    <?= $this->render('tabs/_entry', ['form' => $form, 'model' => $model]) ?>
                    <?= $this->render('tabs/_content', ['form' => $form, 'model' => $model]) ?>
                    <?= $this->render('tabs/_conclusion', ['form' => $form, 'model' => $model]) ?>
                </div>
            </div>
        </div>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
            Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
            . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
            ['class' => 'btn btn-success btn-flat']
        ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
