<?php

use dosamigos\ckeditor\CKEditor;
use emilasp\course\common\models\Course;
use emilasp\media\extensions\FileInputWidget\FileInputWidget;
use emilasp\media\models\File;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\Course */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->errorSummary($model); ?>

        <div class="panel-heading">
            <h3 class="panel-title"><?= Yii::t('media', 'Files') ?></h3>
        </div>
        <div class="panel-body">

            <div class="row">
                <div class="col-md-6">
                    <?php if ($model->image) : ?>
                        <a href="<?= $model->image->getUrl('max') ?>"
                           data-jbox-image="gallery<?= $model->image->id ?>">
                            <img src="<?= $model->image->getUrl('ico') ?>"
                                 alt="<?= $model->image->title ?>" class="img-thumbnail">
                        </a>
                    <?php endif; ?>

                    <?= $form->field($model, 'imageUpload')->fileInput()->label(false) ?>
                </div>
                <div class="col-md-6">
                    <?= FileInputWidget::widget([
                        'model'         => $model,
                        'type'          => FileInputWidget::TYPE_MULTI,
                        'title'         => true,
                        'description'   => true,
                        'showTitle'     => true,
                        'previewHeight' => 20,
                    ]) ?>
                </div>
            </div>

        </div>
        <hr/>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'code')->textInput(['maxlength' => true]) ?>
            </div>
        </div>



        <?= $form->field($model, 'description')->widget(CKEditor::className(), [
            'options'       => ['rows' => 3],
            'preset'        => 'basic',
            'clientOptions' => [
                'filebrowserUploadUrl' => '/media/file/cke-upload'
            ]
        ]) ?>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'views')->textInput() ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'rating')->textInput() ?>
            </div>
        </div>

        <div class="row">
            <div class="col-md-6">
                <?= $form->field($model, 'status')->dropDownList(Course::$statuses) ?>
            </div>
            <div class="col-md-6">
                <?= $form->field($model, 'start_at')->widget(DatePicker::className(), [
                    'options'       => ['placeholder' => 'Выберите дату..'],
                    'language' => 'ru-RU',
                    'pluginOptions' => [
                        'format'         => 'yyyy-mm-dd',
                        'todayHighlight' => true
                    ]
                ]) ?>
            </div>
        </div>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
                Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
                . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
                ['class' => 'btn btn-success btn-flat']
            ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
