<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\Course */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => Yii::t('course', 'Course'),
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('course', 'Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="course-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
