<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\Course */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('course', 'Courses'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
