<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseUserLink */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => Yii::t('course', 'Course User Link'),
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('course', 'Course User Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="course-user-link-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
