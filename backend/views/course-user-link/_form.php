<?php

use emilasp\course\common\models\Course;
use emilasp\course\common\models\CourseUserLink;
use kartik\widgets\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseUserLink */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-user-link-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'course_id')->dropDownList(Course::find()->map()->all()) ?>

        <?= $form->field($model, 'user_id')->textInput() ?>

        <?= $form->field($model, 'pay_id')->textInput() ?>

        <?= $form->field($model, 'status')->dropDownList(CourseUserLink::$statuses) ?>

        <?= $form->field($model, 'start_at')->widget(DatePicker::className(), [
            'options'       => ['placeholder' => 'Выберите дату..'],
            'language' => 'ru-RU',
            'pluginOptions' => [
                'format'         => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) ?>

        <?= $form->field($model, 'end_at')->widget(DatePicker::className(), [
            'options'       => ['placeholder' => 'Выберите дату..'],
            'language' => 'ru-RU',
            'pluginOptions' => [
                'format'         => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) ?>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
                Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
                . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
                ['class' => 'btn btn-success btn-flat']
            ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
