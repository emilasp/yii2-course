<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseUserLink */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('course', 'Course User Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-user-link-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
