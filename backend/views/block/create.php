<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseBlock */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('course', 'Course Blocks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-block-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
