<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseBlock */

$this->title = Yii::t('site', 'Update {modelClass}: ', [
'modelClass' => Yii::t('course', 'Course Block'),
]) . $model->id;
$this->params['breadcrumbs'][] = ['label' => Yii::t('course', 'Course Blocks'), 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->name, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = Yii::t('site', 'Updated');
?>
<div class="course-block-update">

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
