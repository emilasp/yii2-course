<?php

use dosamigos\ckeditor\CKEditor;
use emilasp\course\common\models\Course;
use emilasp\media\models\File;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseBlock */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-block-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->errorSummary($model); ?>

        <?php if ($model->image) : ?>
            <?= Html::a(
                Html::img($model->image->getUrl(File::SIZE_ICO), [
                    'class'    => 'img-thumbnail media-object',
                    'data-src' => $model->image->getUrl(File::SIZE_MAX),
                    'alt'      => $model->image->title,
                ]),
                $model->image->getUrl(File::SIZE_MAX),
                [
                    'data-jbox-image' => 'gl',
                    'data-pjax'       => 0,
                ]
            ) ?>
        <?php else : ?>
            <?= Html::img(File::getNoImageUrl(File::SIZE_ICO), ['class' => 'img-thumbnail media-object']); ?>
        <?php endif ?>

        <?= $form->field($model, 'imageUpload')->fileInput()->label(false) ?>

        <?= $form->field($model, 'course_id')->dropDownList(Course::find()->map()->all()) ?>

        <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

        <?= $form->field($model, 'description')->widget(CKEditor::className(), [
            'options'       => ['rows' => 3],
            'preset'        => 'basic',
            'clientOptions' => [
                'filebrowserUploadUrl' => '/media/file/cke-upload'
            ]
        ]) ?>


        <?= $form->field($model, 'status')->dropDownList(Course::$statuses) ?>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
                Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
                . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
                ['class' => 'btn btn-success btn-flat']
            ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
