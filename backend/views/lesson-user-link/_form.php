<?php

use emilasp\course\common\models\CourseLessonUserLink;
use kartik\date\DatePicker;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseLessonUserLink */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="course-lesson-user-link-form box box-primary">
    <?php $form = ActiveForm::begin(); ?>
    <div class="box-body table-responsive">

        <?= $form->field($model, 'lesson_id')->textInput() ?>

        <?= $form->field($model, 'user_id')->textInput() ?>

        <?= $form->field($model, 'tasks')->textarea(['rows' => 10]) ?>

        <?= $form->field($model, 'score')->textInput() ?>

        <?= $form->field($model, 'status')->dropDownList(CourseLessonUserLink::$statuses) ?>

        <?= $form->field($model, 'start_at')->widget(DatePicker::className(), [
            'options'       => ['placeholder' => 'Выберите дату..'],
            'language' => 'ru-RU',
            'pluginOptions' => [
                'format'         => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) ?>

        <?= $form->field($model, 'end_at')->widget(DatePicker::className(), [
            'options'       => ['placeholder' => 'Выберите дату..'],
            'language' => 'ru-RU',
            'pluginOptions' => [
                'format'         => 'yyyy-mm-dd',
                'todayHighlight' => true
            ]
        ]) ?>

    </div>
    <div class="box-footer text-right">
        <?= Html::submitButton(
                Html::tag('i', '', ['class' => 'fa fa-floppy-o']) . ' '
                . ($model->isNewRecord ? Yii::t('site', 'Create') : Yii::t('site', 'Save')),
                ['class' => 'btn btn-success btn-flat']
            ) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
