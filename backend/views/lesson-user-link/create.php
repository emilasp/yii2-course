<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model emilasp\course\common\models\CourseLessonUserLink */

$this->title = Yii::t('site', 'Created');
$this->params['breadcrumbs'][] = ['label' => Yii::t('course', 'Course Lesson User Links'), 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="course-lesson-user-link-create">

    <?= $this->render('_form', [
    'model' => $model,
    ]) ?>

</div>
