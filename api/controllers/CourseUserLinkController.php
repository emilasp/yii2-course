<?php
namespace emilasp\course\api\controllers;

use emilasp\course\common\models\Course;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;


/**
 * CourseUserLinkController implements the CRUD actions for CourseUserLink model.
 */
class CourseUserLinkController extends Controller
{
    public $modelClass = 'emilasp\course\common\models\CourseUserLink';

    public function behaviors()
    {
        $behaviors                           = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['authenticator']['only']  = ['index', 'view'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['update'], $actions['create'], $actions['delete'], $actions['view']);
        return $actions;
    }

    public function actionIndex()
    {
        $modelClass = $this->modelClass;
        $query      = $modelClass::find()
            ->where(['user_id' => Yii::$app->user->id])
            ->with(['course', 'course.image']);

        return new ActiveDataProvider([
            'query' => $query,
        ]);
    }

    public function actionView($code)
    {
        $code = Yii::$app->request->get('code');

        if (!$course = Course::findOne(['code' => $code])) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        $modelClass = $this->modelClass;
        $query      = $modelClass::find()
            ->where(['user_id' => Yii::$app->user->id, 'course_id' => $course->id])
            ->with(['course', 'course.image']);

        return $query->one();
    }
}
