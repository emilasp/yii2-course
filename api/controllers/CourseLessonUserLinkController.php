<?php
namespace emilasp\course\api\controllers;

use emilasp\course\common\models\Course;
use emilasp\course\common\models\CourseLesson;
use Yii;
use yii\data\ActiveDataProvider;
use yii\filters\auth\HttpBearerAuth;
use yii\rest\Controller;
use yii\web\NotFoundHttpException;


/**
 * CourseLessonUserLinkController implements the CRUD actions for CourseUserLink model.
 */
class CourseLessonUserLinkController extends Controller
{
    public $modelClass = 'emilasp\course\common\models\CourseLessonUserLink';

    public function behaviors()
    {
        $behaviors                           = parent::behaviors();
        $behaviors['authenticator']['class'] = HttpBearerAuth::className();
        $behaviors['authenticator']['only']  = ['view'];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();
        unset($actions['index'], $actions['update'], $actions['create'], $actions['delete'], $actions['view']);
        return $actions;
    }

    public function actionView()
    {
        $code = Yii::$app->request->get('code');

        if (!$lesson = CourseLesson::findOne(['code' => $code])) {
            throw new NotFoundHttpException('Страница не найдена');
        }

        $modelClass = $this->modelClass;
        $query      = $modelClass::find()
            ->where(['user_id' => Yii::$app->user->id, 'lesson_id' => $lesson->id])
            ->with(['lesson', 'lesson.image']);

        $model = $query->one();
        $model->lesson->toJsonField();

        return $model;
    }
}
