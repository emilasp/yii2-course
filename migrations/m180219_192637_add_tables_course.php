<?php

use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m180219_192637_add_tables_course*/
class m180219_192637_add_tables_course extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->createTable('course_course', [
            'id'          => $this->primaryKey(11),
            'code'        => $this->string(100)->unique()->notNull(),
            'image_id'    => $this->integer(11),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'status'      => $this->smallInteger(1)->notNull(),
            'views'       => $this->integer(11)->defaultValue(0),
            'rating'      => $this->integer(11)->defaultValue(0),
            'start_at'    => $this->dateTime(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_course_course_created_by',
            'course_course',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_course_course_updated_by',
            'course_course',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_course_course_image_id',
            'course_course',
            'image_id',
            'media_file',
            'id'
        );

        $this->createIndex('idx_course_course_views', 'course_course', 'views');
        $this->createIndex('idx_course_course_rating', 'course_course', 'rating');


        $this->createTable('course_block', [
            'id'          => $this->primaryKey(11),
            'course_id'   => $this->integer(11)->notNull(),
            'image_id'    => $this->integer(11),
            'name'        => $this->string(255)->notNull(),
            'description' => $this->text()->notNull(),
            'status'      => $this->smallInteger(1)->notNull(),
            'created_at'  => $this->dateTime(),
            'updated_at'  => $this->dateTime(),
            'created_by'  => $this->integer(11),
            'updated_by'  => $this->integer(11),
        ], $this->tableOptions);


        $this->addForeignKey(
            'fk_course_block_course_id',
            'course_block',
            'course_id',
            'course_course',
            'id'
        );
        $this->addForeignKey(
            'fk_course_block_created_by',
            'course_block',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_course_block_updated_by',
            'course_block',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_course_block_image_id',
            'course_block',
            'image_id',
            'media_file',
            'id'
        );

        $this->createTable('course_lesson', [
            'id'        => $this->primaryKey(11),
            'code'      => $this->string(100)->unique()->notNull(),
            'course_id' => $this->integer(11)->notNull(),
            'block_id'  => $this->integer(11)->notNull(),
            'image_id'  => $this->integer(11),
            'name'      => $this->string(255)->notNull(),

            'entry'      => $this->text()->comment('Вступление(лид абзац - зацепить)'),
            'content'    => $this->text()->comment('Содержание'),
            'conclusion' => $this->text()->comment('Заключение(итоги/выводы)'),

            'entry_media'      => 'JSONB default \'{}\'',
            'content_media'    => 'JSONB default \'{}\'',
            'conclusion_media' => 'JSONB default \'{}\'',

            'tasks'        => 'JSONB default \'{}\'',
            'views'        => $this->integer(11)->defaultValue(0),
            'rating'       => $this->integer(11)->defaultValue(0),
            'type'         => $this->smallInteger(1)->notNull(), //Урок, тест, игра
            'type_content' => $this->smallInteger(1)->notNull(), //Video, audio, mixed, text
            'status'       => $this->smallInteger(1)->notNull(),
            'created_at'   => $this->dateTime(),
            'updated_at'   => $this->dateTime(),
            'created_by'   => $this->integer(11),
            'updated_by'   => $this->integer(11),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_course_lesson_course_id',
            'course_lesson',
            'course_id',
            'course_course',
            'id'
        );
        $this->addForeignKey(
            'fk_course_lesson_block_id',
            'course_lesson',
            'block_id',
            'course_block',
            'id'
        );
        $this->addForeignKey(
            'fk_course_lesson_created_by',
            'course_lesson',
            'created_by',
            'users_user',
            'id'
        );
        $this->addForeignKey(
            'fk_course_lesson_updated_by',
            'course_lesson',
            'updated_by',
            'users_user',
            'id'
        );

        $this->addForeignKey(
            'fk_course_lesson_image_id',
            'course_lesson',
            'image_id',
            'media_file',
            'id'
        );


        $this->createTable('course_course_user_link', [
            'id'        => $this->primaryKey(11),
            'course_id' => $this->integer(11)->notNull(),
            'user_id'   => $this->integer(11)->notNull(),
            'pay_id'    => $this->integer(11),
            'status'    => $this->smallInteger(1)->notNull(), //Идет, завершен, закрыт, ожидает оплаты
            'start_at'  => $this->dateTime(),
            'end_at'    => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_course_course_user_link_course_id',
            'course_course_user_link',
            'course_id',
            'course_course',
            'id'
        );

        $this->addForeignKey(
            'fk_course_course_user_link_user_id',
            'course_course_user_link',
            'user_id',
            'users_user',
            'id'
        );

        $this->createTable('course_lesson_user_link', [
            'id'        => $this->primaryKey(11),
            'lesson_id' => $this->integer(11)->notNull(),
            'user_id'   => $this->integer(11)->notNull(),
            'tasks'     => 'JSONB default \'{}\'',
            'score'     => $this->integer(11),
            'status'    => $this->smallInteger(1)->notNull(), //Идет, завершен, закрыт, ожидает оплаты
            'start_at'  => $this->dateTime(),
            'end_at'    => $this->dateTime(),
        ], $this->tableOptions);

        $this->addForeignKey(
            'fk_course_lesson_user_link_lesson_id',
            'course_lesson_user_link',
            'lesson_id',
            'course_lesson',
            'id'
        );

        $this->addForeignKey(
            'fk_course_lesson_user_link_user_id',
            'course_lesson_user_link',
            'user_id',
            'users_user',
            'id'
        );

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->dropTable('course_lesson_user_link');
        $this->dropTable('course_course_user_link');
        $this->dropTable('course_lesson');
        $this->dropTable('course_block');
        $this->dropTable('course_course');

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }
}
