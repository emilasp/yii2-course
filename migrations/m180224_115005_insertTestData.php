<?php

use emilasp\course\common\models\Course;
use emilasp\course\common\models\CourseBlock;
use emilasp\course\common\models\CourseLesson;
use emilasp\course\common\models\CourseLessonUserLink;
use emilasp\course\common\models\CourseUserLink;
use yii\db\Migration;
use emilasp\core\helpers\FileHelper;

/**
 * Class m180224_115005_insertTestData*/
class m180224_115005_insertTestData extends Migration
{
    private $tableOptions = null;
    private $time;
    private $memory;

    /**
     * UP
     */
    public function up()
    {
        $this->insertCourses();

        $this->afterMigrate();
    }

    /**
     * DOWN
     */
    public function down()
    {
        $this->db->createCommand('TRUNCATE TABLE course_course CASCADE;')->execute();

        $this->afterMigrate();
    }


    /**
     * Initializes the migration.
     * This method will set [[db]] to be the 'db' application component, if it is null.
     */
    public function init()
    {
        parent::init();
        $this->setTableOptions();
        $this->beforeMigrate();
    }

    /**
     * Устанавливаем дефолтные параметры для таблиц
     */
    private function setTableOptions()
    {
        if ($this->db->driverName === 'mysql') {
            $this->tableOptions = 'ENGINE=InnoDB  DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci';
        }
    }

    /**
     * Устанавливаем начальные параметры времени и памяти
     */
    private function beforeMigrate()
    {
        echo 'Start..' . PHP_EOL;
        $this->memory = memory_get_usage();
        $this->time   = microtime(true);
    }

    /**
     * Выводим параметры времени и памяти
     */
    private function afterMigrate()
    {
        echo 'End..' . PHP_EOL;
        echo 'Использовано памяти: ' . FileHelper::formatSizeUnits((memory_get_usage() - $this->memory)) . PHP_EOL;
        echo 'Время выполнения скрипта: ' . (microtime(true) - $this->time) . ' сек.' . PHP_EOL;
    }

    /**
     * Create courses
     */
    private function insertCourses(): void
    {
        $items = $this->getTestData();

        foreach ($items as $item) {
            echo 'Create course: ' . $item['name'];

            $blocks = $item['blocks'];
            unset($item['blocks']);
            $links = $item['links'];
            unset($item['links']);

            $course = new Course($item);
            $course->save();

            $this->createBlocks($course, $blocks);
            $this->createCourseUserLinks($course, $links);
        }
    }

    /**
     * Create Blocks
     *
     * @param $course
     * @param $blocks
     */
    private function createBlocks($course, $blocks): void
    {
        foreach ($blocks as $item) {
            echo 'Create block: ' . $item['name'];
            $lessons = $item['lessons'];
            unset($item['lessons']);

            $block            = new CourseBlock($item);
            $block->course_id = $course->id;
            $block->save();

            $this->createLessons($course, $block, $lessons);
        }
    }

    /**
     * Create lessons
     *
     * @param $course
     * @param $block
     * @param $lessons
     */
    private function createLessons($course, $block, $lessons): void
    {
        foreach ($lessons as $item) {
            echo 'Create lesson: ' . $item['name'];

            if ($links = $item['links'] ?? null) {
                unset($item['links']);
            }

            $lesson            = new CourseLesson($item);
            $lesson->course_id = $course->id;
            $lesson->block_id  = $block->id;
            $lesson->save();

            if ($links) {
                $this->createLessonUserLinks($lesson, $links);
            }
        }
    }


    private function createLessonUserLinks($lesson, $links): void
    {
        foreach ($links as $item) {
            echo 'Create lesson user link: ' . $item['user_id'];

            $link            = new CourseLessonUserLink($item);
            $link->lesson_id = $lesson->id;
            $link->user_id   = $item['user_id'];
            $link->status    = $item['status'];
            $link->save();
        }
    }

    private function createCourseUserLinks($course, $links): void
    {
        foreach ($links as $item) {
            echo 'Create course user link: ' . $item['user_id'];

            $link            = new CourseUserLink($item);
            $link->course_id = $course->id;
            $link->user_id   = $item['user_id'];
            $link->status    = $item['status'];
            $link->save();
        }
    }

    /**
     * Get data
     *
     * @return array
     */
    private function getTestData(): array
    {
        return [
            [
                'name'        => 'Устный счет',
                'description' => '<p><b>Цифровые вертушки</b> на телефонной матрице.
</p><p>Цифровые вертушки в базовом варианте представляют собой две телефонных панели, допускающие повороты вокруг
    центральной оси. Цифровые вертушки являются механическими учебными пособиями, позволяющими в игровой форме изучать с
    детьми методы геометрического сложения и умножения однозначных десятичных чисел.
    Описаны в патенте РФ<sup id="cite_ref-2" class="reference"><a href="#cite_note-2">&#91;2&#93;</a></sup>.
</p><p><b>Конструкция цифровой вертушки</b>. Неподвижная основа вертушки представляет собой плоскость с рисунками цифр,
    расставленных в формате Т-матрицы из трех строк и трех столбцов. На основу накладывается поворачивающаяся плоскость
    (пропеллер) на которой нарисованы стрелочки, подсказывающие ответы. Ось вращения пропеллера совпадает с центром
    неподвижной Т-матрицы. Единственное доступное движение&#160;— это поворот пропеллера вокруг оси<sup id="cite_ref-3"
                                                                                                        class="reference"><a
                href="#cite_note-3">&#91;3&#93;</a></sup>.
</p><p><b>Сложение</b>.
</p><p>Принцип действия цифровой вертушки заключается в следующем. Запишем сумму однозначых чисел A+B=[D;E] двумя
    цифрами десятков D и единиц Е. Все примеры с одинаковой величиной слагаемого +B назовём <i>листом сложения</i>.
</p><p>Цифру единиц E примера сложения показываем стрелочкой от A к E. Эта стрелочка называется <i>указателем единиц
        суммы</i>.
</p><p>Стрелочки на листе сложения образуют ломаные линии <i>молний</i>.
</p><p><b>Правило единиц</b>. Сложение A+B выполняется путём перехода по стрелочке-указателю, изображённой на листе
    сложения (+B), от цифры A к цифре E единиц суммы.
</p><p><i>Пример 2+1</i>. Потребуется лист сложения (+1). Установим фишку-метку на цифру 2 на T-матрице. Перемещаем
    фишку по стрелочке молнии, выходящей из точки 2. Конец указателя показывает сумму 3.
</p><p><i>Пример 7+7</i>. Берём лист сложения (+7). Установим фишку-метку на цифру 7 на T-матрице. Перемещаем фишку по
    стрелочке «шаг вверх» на 7-й молнии, выходящей из точки A=7. Конец указателя показывает цифру единиц E=4.
</p><p>Применяем <b>правило десятков</b>. <i>Если на указателе единиц суммы A-&gt;E есть инверсия, то есть, A&gt;E,
        тогда цифра десятков суммы D=1</i><sup id="cite_ref-4" class="reference"><a href="#cite_note-4">&#91;4&#93;</a></sup>.
</p>',
                'status'      => Course::STATUS_ENABLED,
                'start_at'    => '2018-01-12 00:00:00',
                'links'    => [['user_id' => 1, 'status' => CourseUserLink::STATUS_WORK]],
                'blocks'      => [
                    [
                        'name'        => 'Неделя 1',
                        'description' => 'Самая первая неделя обучения. Вводные заняти. Терминология.',
                        'status'      => CourseBlock::STATUS_ENABLED,
                        'lessons'     => [
                            [
                                'name'         => 'Урок 1',
                                'entry'        => 'Цифровые вертушки',
                                'content'      => '<p>Указание радиальных лучей при умножении на 3 можно выполнить ладонью <i>правой руки</i>.
Отставим в сторону большой палец правой руки, плотно сжав остальные пальцы.
Положим правую ладонь на центр Т-матрицы, направив большой палец на множитель B.
Тогда остальные пальцы правой руки покажут цифру единиц E произведения 3xB=[D;E]).
Итак, умножение на 3 реализуется на телефонной матрице <i>правилом правой руки</i>".
Например, 3x2=6<sup id="cite_ref-6" class="reference"><a href="#cite_note-6">&#91;6&#93;</a></sup>.
</p><p>Аналогично: правило единиц умножения на 7&#160;— это <i>правило левой руки</i><sup id="cite_ref-7" class="reference"><a href="#cite_note-7">&#91;7&#93;</a></sup>.
</p><p>Правило единиц умножения на 9&#160;— это <i>шпагат из пальцев</i><sup id="cite_ref-8" class="reference"><a href="#cite_note-8">&#91;8&#93;</a></sup>.
</p>',
                                'conclusion'   => '<p>Другие геометрические правила единиц умножения можно показать на схемах, на которых имеются радиальные лучи Т-матрицы<sup id="cite_ref-9" class="reference"><a href="#cite_note-9">&#91;9&#93;</a></sup>.
При этом умножение чётных чисел выполняется на чётном кресте цифр Т-матрицы<sup id="cite_ref-10" class="reference"><a href="#cite_note-10">&#91;10&#93;</a></sup>.
Удачным тренажёром являются механические учебные пособия&#160;— цифровые вертушки, использующие цифровую телефонную матрицу<sup id="cite_ref-11" class="reference"><a href="#cite_note-11">&#91;11&#93;</a></sup>.
</p>',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                                'links'    => [['user_id' => 1, 'status' => CourseLessonUserLink::STATUS_END]],
                            ],
                            [
                                'name'         => 'Урок 2',
                                'entry'        => 'Стрелочки на листе сложения',
                                'content'      => '',
                                'conclusion'   => '',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                                'links'    => [['user_id' => 1, 'status' => CourseLessonUserLink::STATUS_END]],
                            ],
                            [
                                'name'         => 'Урок 3',
                                'entry'        => 'Правило поворота лучей',
                                'content'      => '',
                                'conclusion'   => '',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                                'links'    => [['user_id' => 1, 'status' => CourseLessonUserLink::STATUS_END]],
                            ],
                            [
                                'name'         => 'Урок 4',
                                'entry'        => 'Указание радиальных лучей при умножении на 3',
                                'content'      => '',
                                'conclusion'   => '',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                                'links'    => [['user_id' => 1, 'status' => CourseLessonUserLink::STATUS_END]]
                            ],

                        ]
                    ],
                    [
                        'name'        => 'Неделя 2',
                        'description' => 'Вторая неделя обучения',
                        'status'      => CourseBlock::STATUS_ENABLED,
                        'lessons'     => [
                            [
                                'name'         => 'Урок 5',
                                'entry'        => 'Правило единиц умножения на 9',
                                'content'      => '',
                                'conclusion'   => '',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                                'links'    => [['user_id' => 1, 'status' => CourseLessonUserLink::STATUS_END]]
                            ],
                            [
                                'name'         => 'Урок 6',
                                'entry'        => 'Процесс устного счёта',
                                'content'      => '',
                                'conclusion'   => '',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                                'links'    => [['user_id' => 1, 'status' => CourseLessonUserLink::STATUS_END]]
                            ],
                            [
                                'name'         => 'Урок 7',
                                'entry'        => 'Супервычислители, демонстрируя высокие скорости мышления, используют свои визуальные способности и отличную зрительную память',
                                'content'      => '<p>Указание радиальных лучей при умножении на 3 можно выполнить ладонью <i>правой руки</i>.
Отставим в сторону большой палец правой руки, плотно сжав остальные пальцы.
Положим правую ладонь на центр Т-матрицы, направив большой палец на множитель B.
Тогда остальные пальцы правой руки покажут цифру единиц E произведения 3xB=[D;E]).
Итак, умножение на 3 реализуется на телефонной матрице <i>правилом правой руки</i>".
Например, 3x2=6<sup id="cite_ref-6" class="reference"><a href="#cite_note-6">&#91;6&#93;</a></sup>.
</p><p>Аналогично: правило единиц умножения на 7&#160;— это <i>правило левой руки</i><sup id="cite_ref-7" class="reference"><a href="#cite_note-7">&#91;7&#93;</a></sup>.
</p><p>Правило единиц умножения на 9&#160;— это <i>шпагат из пальцев</i><sup id="cite_ref-8" class="reference"><a href="#cite_note-8">&#91;8&#93;</a></sup>.
</p>',
                                'conclusion'   => '<p>Другие геометрические правила единиц умножения можно показать на схемах, на которых имеются радиальные лучи Т-матрицы<sup id="cite_ref-9" class="reference"><a href="#cite_note-9">&#91;9&#93;</a></sup>.
При этом умножение чётных чисел выполняется на чётном кресте цифр Т-матрицы<sup id="cite_ref-10" class="reference"><a href="#cite_note-10">&#91;10&#93;</a></sup>.
Удачным тренажёром являются механические учебные пособия&#160;— цифровые вертушки, использующие цифровую телефонную матрицу<sup id="cite_ref-11" class="reference"><a href="#cite_note-11">&#91;11&#93;</a></sup>.
</p>',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                                'links'    => [['user_id' => 1, 'status' => CourseLessonUserLink::STATUS_WORK]]
                            ],
                            [
                                'name'         => 'Урок 8',
                                'entry'        => 'Выработка навыков устного счёта занимает особое место',
                                'content'      => '',
                                'conclusion'   => '',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                            ],

                        ]
                    ],
                    [
                        'name'        => 'Неделя 3',
                        'description' => 'Третья неделя обучения',
                        'status'      => CourseBlock::STATUS_ENABLED,
                        'lessons'     => [
                            [
                                'name'         => 'Урок 9',
                                'entry'        => 'Цифровые вертушки в базовом варианте представляют собой две телефонных панели',
                                'content'      => '',
                                'conclusion'   => '',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                            ],
                            [
                                'name'         => 'Урок 10',
                                'entry'        => 'Принцип действия цифровой вертушки',
                                'content'      => '',
                                'conclusion'   => '',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                            ],
                            [
                                'name'         => 'Урок 11',
                                'entry'        => 'Применяем правило десятков',
                                'content'      => '',
                                'conclusion'   => '',
                                'type'         => CourseLesson::TYPE_LESSON,
                                'type_content' => CourseLesson::TYPE_CONTENT_MIXED,
                                'status'       => CourseLesson::STATUS_ENABLED,
                            ],
                        ]
                    ],
                ]
            ],
            [
                'name'        => 'Творческое подсознание',
                'description' => '',
                'status'      => Course::STATUS_ENABLED,
                'start_at'    => '2018-01-25 00:00:00',
                'links'    => [['user_id' => 1, 'status' => CourseUserLink::STATUS_WORK]],
                'blocks'      => []
            ],
            [
                'name'        => 'Похудение',
                'description' => 'Похудение с помощью аутогенных тренировок',
                'status'      => Course::STATUS_ENABLED,
                'start_at'    => '2018-01-25 00:00:00',
                'links'    => [['user_id' => 1, 'status' => CourseUserLink::STATUS_WORK]],
                'blocks'      => []
            ],
            [
                'name'        => 'Скорочтение',
                'description' => '',
                'status'      => Course::STATUS_ENABLED,
                'start_at'    => '2018-01-25 00:00:00',
                'links'    => [['user_id' => 1, 'status' => CourseUserLink::STATUS_WORK]],
                'blocks'      => []
            ]
        ];
    }
}

?>

